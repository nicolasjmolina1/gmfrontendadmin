let idEvento = window.location.href;
let evento
const formulario = document.getElementById('formuario-actualizar-evento')
const inputTitle = document.getElementById('inputTitle')
const inputDescription = document.getElementById('inputDescription')
const inputFecha = document.getElementById('datepicker')
const inputHora = document.getElementById('timepicker')



document.addEventListener('DOMContentLoaded', () =>{
    idEvento = idEvento.replace("http://127.0.0.1:5500/sections/evento/evento-actualizar.html?","");
    console.log("Id del evento:" + idEvento + "");
    obteneEventoDetalle()
})

formulario.addEventListener('click', e => {
    e.preventDefault() 
    validarFormulario(e)
     //btnActualizarAction(e)
 })


const obteneEventoDetalle = async() => {
    try{
        const respuesta = await fetch('http://localhost:8080/evento/buscar/'+idEvento)
        const data = await respuesta.json()
        console.log('RESPUESTA: '+ JSON.stringify(data))
        parseEvento(data)
    }catch(error){
        console.error()
    }
}

const parseEvento = data => {
    evento = {
        id: data.id,
        title: data.title,
        fecha: data.fecha,
        hora: data.hora,
        foto: data.foto,
        descripcion: data.descripcion
    }
    console.log(evento)
    mostrarEvento(evento)
}

const mostrarEvento = evento => {
    inputTitle.value = evento.title
    inputDescription.value = evento.descripcion
    inputFecha.value= evento.fecha
    inputHora.value = evento.hora
}

const validarFormulario = e => {
    //obtener el valor de los inputs creando un nuevo objeto evento y pasar eso y el evento a la function actualizar
    const eventoModificado = {
        id: idEvento,
        title: inputTitle.value,
        fecha: inputFecha.value,
        hora: inputHora.value,
        foto: evento.foto,
        descripcion: inputDescription.value
    }
    btnActualizarAction(e, eventoModificado)
}

const btnActualizarAction = (e, eventoModificado) => {
    if(e.target.classList.contains('btn-success')){
        console.log('click me', eventoModificado.descripcion)
        actualizarEvento(eventoModificado)
    }
}


const actualizarEvento = async (eventoModificado) => {
    try{
        const respuesta = await fetch('http://localhost:8080/evento/actualizar', {
           method: 'PUT', // or 'PUT'
           mode: 'cors',
           body: JSON.stringify(eventoModificado), // data can be `string` or {object}!
           headers:{
             'Content-Type': 'application/json'
           }
         })
        const data = await respuesta.json()
        console.log(data)
        window.location.replace("http://127.0.0.1:5500/sections/evento/evento-lista.html");
       //  var hello = "hi"
       //  window.location.replace("http://127.0.0.1:5500/sections/evento/evento-lista.html?"+hello+"");

    }catch(error){
        console.error()
    }
}