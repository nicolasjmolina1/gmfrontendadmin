const formulario = document.getElementById('formuario-crear-evento')
const inputTitle = document.getElementById('inputTitle')
const inputDescription = document.getElementById('inputDescription')
const inputFecha = document.getElementById('datepicker')
const inputHora = document.getElementById('timepicker')



document.addEventListener('DOMContentLoaded', () => {
    procesarFormulario()

})


const procesarFormulario = () => {
    formulario.addEventListener('submit', e => {
        e.preventDefault()
        console.log('procesando formulario')
        crearEvento()
    })
}

const crearEvento = () => {
    //validar campos vacios
    const evento = {
        title: inputTitle.value,
        fecha: inputFecha.value,
        hora: inputHora.value,
        descripcion: inputDescription.value
    }
    console.log(evento)
    guardarEvento(evento)
}

const guardarEvento = async(evento) => {
     try{
         const respuesta = await fetch('http://localhost:8080/evento/crear', {
            method: 'POST', // or 'PUT'
            mode: 'cors',
            body: JSON.stringify(evento), // data can be `string` or {object}!
            headers:{
              'Content-Type': 'application/json'
            }
          })
         const data = await respuesta.json()
         console.log(data)
         window.location.replace("http://127.0.0.1:5500/sections/evento/evento-lista.html");
        //  var hello = "hi"
        //  window.location.replace("http://127.0.0.1:5500/sections/evento/evento-lista.html?"+hello+"");

     }catch(error){
         console.error()
     }
}
