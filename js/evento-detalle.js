let idEvento = window.location.href;
let evento
const eventoDetalleContainer = document.getElementById('evento-detalle-container')
const templateEventoDetalle = document.getElementById('template-evento-detalle').content
const fragment = document.createDocumentFragment()


document.addEventListener('DOMContentLoaded', () =>{
    idEvento = idEvento.replace("http://127.0.0.1:5500/sections/evento/evento-detalle.html?","");
    console.log("Id del evento:" + idEvento + "");
    obteneEventoDetalle()
})

eventoDetalleContainer.addEventListener('click', e => {
    btnAccion(e)
})


const obteneEventoDetalle = async() => {
    try{
        const respuesta = await fetch('http://localhost:8080/evento/buscar/'+idEvento)
        const data = await respuesta.json()
        console.log('RESPUESTA: '+ JSON.stringify(data))
        parseEvento(data)
    }catch(error){
        console.error()
    }
}

const parseEvento = data => {
    evento = {
        id: data.id,
        title: data.title,
        fecha: data.fecha,
        hora: data.hora,
        foto: data.foto,
        descripcion: data.descripcion
    }
    console.log(evento)
    mostrarEvento(evento)
}

const mostrarEvento = evento => {
    templateEventoDetalle.querySelector('.card-title').textContent = evento.title
    templateEventoDetalle.querySelector('.card-text').textContent = evento.descripcion
    templateEventoDetalle.getElementById('evento-fecha-hora').textContent = 'Fecha del evento '+evento.fecha+' - '+evento.hora+'hs'

    const clone = templateEventoDetalle.cloneNode(true)
    fragment.appendChild(clone)
    eventoDetalleContainer.appendChild(fragment)
}

const btnAccion = e => {
    if(e.target.classList.contains('btn-primary')){
        e.preventDefault()
        console.log('actualizar: '+idEvento)
        window.location.replace("http://127.0.0.1:5500/sections/evento/evento-actualizar.html?"+idEvento)
    }
    if(e.target.classList.contains('btn-danger')){
    e.preventDefault()
    console.log('ELIMINAR: '+ idEvento)
    confirmDelete(idEvento)
    //window.location.replace("http://127.0.0.1:5500/sections/evento/evento-actualizar.html?"+idEvento)
    }
}

const confirmDelete = idEvento => {
const btnConfirmDelete = document.getElementById('btn-confirm')
    btnConfirmDelete.addEventListener('click', function(e) {
        if(e.target.classList.contains('btn-success')){
            e.preventDefault()
            console.log('EVENTO ELIMINADO: '+ idEvento)
            deleteEvento(idEvento)
        }
    })
}

const deleteEvento = async(idEvento) => {
    try{
        const respuesta = await fetch('http://localhost:8080/evento/eliminar/'+idEvento, {
           method: 'DELETE', // or 'PUT'
           mode: 'cors',
           body: idEvento, // data can be `string` or {object}!
           headers:{
             'Content-Type': 'application/json'
           }
         })
        const data = await respuesta.json()
        console.log('Respuesta delete: '+data)
        window.location.replace("http://127.0.0.1:5500/sections/evento/evento-lista.html");

    }catch(error){
        console.error()
    }
}
