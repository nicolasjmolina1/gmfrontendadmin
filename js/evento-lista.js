const templateListaEventos = document.getElementById('lista-eventos-template').content
const fragment = document.createDocumentFragment()
const cardsEventos = document.getElementById('cards-eventos')

const templateAlertEvento = document.getElementById('event-alert-template').content
const eventAlert = document.getElementById('event-alert')

const fechaInicio = document.getElementById('datepicker')
const fechaFin = document.getElementById('datepickerend')
const btnBuscar = document.getElementById('btn-buscar')


document.addEventListener('DOMContentLoaded', () =>{
    obtenerEventos()
})

cardsEventos.addEventListener('click', function(e) {
    btnAccion(e, this.id);
});

btnBuscar.addEventListener('click', e => {
    buscarPorFecha(e)
})

const obtenerEventos = async() => {
    try{
        const respuesta = await fetch('http://localhost:8080/evento/listado-eventos')
        const data = await respuesta.json()
        console.log('RESPUESTA: '+ data)
        verifyLengthResponse(data)
    }catch(error){
        console.error()
    }
}

const verifyLengthResponse = data => {
    if(Object.values(data).length > 0){
        pintarEventos(data)
    }else{
        const clone = templateAlertEvento.cloneNode(true)
        fragment.appendChild(clone)
        eventAlert.appendChild(fragment)
    }
}

const pintarEventos = eventos => {
    cardsEventos.innerHTML = ''
    Object.values(eventos).forEach(e => {
            console.log(e)
            templateListaEventos.getElementById('event-title').textContent = e.title
            templateListaEventos.getElementById('event-fecha').textContent = 'Fecha del evento '+e.fecha+' - '+e.hora+'hs'
            console.log(e.id)
            templateListaEventos.querySelector('.card-body').dataset.id = e.id
            const clone = templateListaEventos.cloneNode(true)
            fragment.appendChild(clone)
        
    })
    cardsEventos.appendChild(fragment)
}

const btnAccion = (e, id) => {
    let idEvento = e.target.parentElement.dataset.id
    if(e.target.classList.contains('btn-info')){
        e.preventDefault()
        console.log('VER MAS: '+ e.target.parentElement.className)//e.target.parentElement.className
        window.location.replace("http://127.0.0.1:5500/sections/evento/evento-detalle.html?"+idEvento)
     }

    if(e.target.classList.contains('btn-primary')){
        e.preventDefault()
        console.log('ACTUALIZAR: '+ e.target.parentElement.className)
        window.location.replace("http://127.0.0.1:5500/sections/evento/evento-actualizar.html?"+idEvento)
    }

    if(e.target.classList.contains('btn-danger')){
        e.preventDefault()
        console.log('ELIMINAR: '+ idEvento)
        confirmDelete(idEvento)
    }
}

const confirmDelete = idEvento => {
    const btnConfirmDelete = document.getElementById('btn-confirm')
        btnConfirmDelete.addEventListener('click', function(e) {
            e.preventDefault()
            if(e.target.classList.contains('btn-success')){
                e.preventDefault()
                console.log('EVENTO ELIMINADO: '+ idEvento)
                deleteEvento(idEvento)
            }
        })
}

const deleteEvento = async(idEvento) => {
    try{
        const respuesta = await fetch('http://localhost:8080/evento/eliminar/'+idEvento, {
           method: 'DELETE', // or 'PUT'
           mode: 'cors',
           body: idEvento, // data can be `string` or {object}!
           headers:{
             'Content-Type': 'application/json'
           }
         })
        const data = await respuesta.json()
        console.log('Respuesta delete: '+data)
        window.location.replace("http://127.0.0.1:5500/sections/evento/evento-lista.html");

    }catch(error){
        console.error()
    }
}


const buscarPorFecha = e => {
    e.preventDefault()
    let inicio = fechaInicio.value
    inicio = inicio.replace(/[^a-zA-Z0-9]/g, '-')
    let fin = fechaFin.value
    fin = fin.replace(/[^a-zA-Z0-9]/g, '-')
    console.log('clickkkkkkkk'+inicio+'/'+fin)
    let inicioParse = new Date(inicio);
    let finParse = new Date(fin)
    buscarEventosPorFecha(inicioParse, finParse)
}

const buscarEventosPorFecha = async(inicioParse, finParse) => {
    try{
        const respuesta = await fetch('http://localhost:8080/evento/filtrar/'+inicioParse+'/'+finParse)
        const data = await respuesta.json()
        console.log('RESPUESTA fechas: '+ data)
        verifyLengthResponse(data)
    }catch(error){
        console.error()
    }
}